from importlib import import_module

import yaml


def get_os_release(image_yaml_path: str):
    with open(image_yaml_path, "rt") as stream:
        file_content = yaml.safe_load(stream)

    os_release = file_content["system"]["release"]

    return os_release


def handle(
    zipfile_handler,
    template_opts,
    cmdb_host,
    skip_handle_os=False,
    single_network_file=False,
):
    tpl_deb = import_module(
        "django_cdstack_tpl_deb_"
        + template_opts["os_release"]
        + ".django_cdstack_tpl_deb_"
        + template_opts["os_release"]
        + ".views"
    )

    tpl_deb.handle(
        zipfile_handler,
        template_opts,
        cmdb_host,
        single_network_file=single_network_file,
    )

    return True
